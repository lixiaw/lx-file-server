package com.weyoung.lxfileserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LxFileServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LxFileServerApplication.class, args);
    }

}
