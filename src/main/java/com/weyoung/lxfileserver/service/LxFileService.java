package com.weyoung.lxfileserver.service;


import com.weyoung.common.pojo.FileInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 文件相关操作处理接口
 *
 * @author Mr.wang
 * @fileName LxFileService
 * @description
 * @date 2020/4/2 13:15
 */
public interface LxFileService {
    /**
     * 文件上传接口
     *
     * @param request
     * @param directoryPrefix 上传目录前缀
     * @return 返回文件名，文件大小，文件路径等信息
     */
    List<FileInfo> upload(HttpServletRequest request, String directoryPrefix);

    /**
     * 图片显示接口
     *
     * @param filePath        文件路径
     * @param isThumbnail     是否缩略图
     * @param directoryPrefix 上传目录前缀
     * @param response
     */
    void showPicture(String filePath, boolean isThumbnail, String directoryPrefix, HttpServletResponse response);

    /**
     * 文件下载接口
     *
     * @param fileFullName 文件全名，包含路径名
     * @param fileName     文件名
     * @param response
     */
    void download(String fileFullName, String fileName, HttpServletResponse response);

    /**
     * 删除文件
     *
     * @param fileInfoMap     文件信息，（key, value）指的是（fileName, filePath）
     * @param directoryPrefix 上传目录前缀
     */
    void delete(Map<String, String> fileInfoMap, String directoryPrefix);
}
