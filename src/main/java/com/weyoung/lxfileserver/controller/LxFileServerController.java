package com.weyoung.lxfileserver.controller;

import com.alibaba.fastjson.JSONObject;
import com.weyoung.common.pojo.FileInfo;
import com.weyoung.common.system.exception.BusinessException;
import com.weyoung.common.web.Message;
import com.weyoung.lxfileserver.service.LxFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * 独立文件服务器-控制器
 *
 * @author Mr.wang
 * @fileName LxFileServerController
 * @description 提供图片文件保存缩略图、文件及图片的上传下载等
 * @date 2020/4/2 12:58
 */
@Controller
@RequestMapping(value = "/lx_file")
public class LxFileServerController {

    @Autowired
    LxFileService lxFileService;

    /**
     * 上传文件并返回文件大小、文件名等信息
     *
     * @param request         HttpServletRequest
     * @param directoryPrefix directoryPrefix
     * @return Message 返回文件信息
     */
    @RequestMapping(value = "/upload", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Message upload(HttpServletRequest request, String directoryPrefix) {
        List<FileInfo> infos = lxFileService.upload(request, directoryPrefix);
        return Message.success(infos);
    }

    /**
     * 查看文件
     *
     * @param filePath        文件路径
     * @param isThumbnail     是否缩略图
     * @param response        HttpServletResponse
     * @param directoryPrefix 目录前缀
     */
    @GetMapping("/show")
    public void show(@RequestParam("filePath") String filePath,
                     @RequestParam("isThumbnail") boolean isThumbnail,
                     @RequestParam("directoryPrefix") String directoryPrefix,
                     HttpServletResponse response) {
        try {
            lxFileService.showPicture(filePath, isThumbnail, directoryPrefix, response);
        } catch (Exception e) {
            throw new BusinessException("获取图片失败！", e);
        }
    }

    /**
     * 查看文件-缩略图
     *
     * @param filePath        文件路径
     * @param directoryPrefix 目录前缀
     * @param response        HttpServletResponse
     */
    @RequestMapping("/show_thumbnail")
    public void showThumbnail(@RequestParam("filePath") String filePath,
                              @RequestParam("directoryPrefix") String directoryPrefix,
                              HttpServletResponse response) {
        try {
            lxFileService.showPicture(filePath, true, directoryPrefix, response);
        } catch (Exception e) {
            throw new BusinessException("获取图片失败！", e);
        }
    }

    /**
     * 下载文件
     *
     * @param fileFullName 文件全名
     * @param fileName     文件名
     * @param response     HttpServletResponse
     */
    @GetMapping("/download")
    public void download(@RequestParam("fileFullName") String fileFullName,
                         @RequestParam("fileName") String fileName,
                         HttpServletResponse response) {
        lxFileService.download(fileFullName, fileName, response);
    }

    /**
     * 删除文件
     *
     * @param fileInfo        文件信息
     * @param directoryPrefix 目录前缀
     * @return 返回删除结果
     */
    @DeleteMapping(value = "/delete", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Message delete(@RequestParam("fileInfo") String fileInfo, @RequestParam("directoryPrefix") String directoryPrefix) {
        try {
            String decode = URLDecoder.decode(fileInfo, StandardCharsets.UTF_8.toString());
            Map<String, String> fileInfoMap = JSONObject.parseObject(decode, Map.class);
            lxFileService.delete(fileInfoMap, directoryPrefix);
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("解码或者转换数据失败", e);
        }
        return Message.success();
    }
}
