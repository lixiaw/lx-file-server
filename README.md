# lx-file-server

#### 介绍
文件服务器；
上传、查看、删除、下载等操作

#### 软件架构
SpringBoot


#### 安装教程

打包命令： <code>mvn clean package</code>
启动jar包命令：
1. <code>nohup java -jar lx-file-server.jar &</code>
//  nohup 意思是不挂断运行命令,当账户退出或终端关闭时,程序仍然运行
//  当用 nohup 命令执行作业时，缺省情况下该作业的所有输出被重定向到nohup.out的文件中
//  除非另外指定了输出文件。
2. <code>java -Dfile.encoding=UTF-8 -jar lx-file-server.jar</code>
3. <code>nohup java -jar lx-file-server.jar >lx-file-server.txt &</code>
//  这种方法会把日志文件输入到你指定的文件中，没有则会自动创建

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
